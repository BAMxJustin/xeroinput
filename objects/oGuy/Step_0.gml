if( command.left.down){
	image_index = 3;
	x -= 6;
}
if( command.right.down){
	image_index = 1;
	x += 6;
}
if( command.up.down){
	image_index = 2;
	y -= 6;
}
if( command.down.down){
	image_index = 0;
	y += 6;
}

// Will change image_index once when the turn key is pressed
if( command.turn.pressed){
	++image_index;
}

// Will show a menu once as soon as the menu key is released
if( command.menu.released){
	show_message( "Rebound turn's primary slot from vk_space to mb_middle");
	command.turn.rebindKey( 0, 1, mb_middle);
}