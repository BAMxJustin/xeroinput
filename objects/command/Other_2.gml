/// @description DELETE THIS - For demonstration purposes only
// Creates hotkeys
// Could be done in separate object by using command.add, etc

add(
	"left",
	"right",
	"up",
	"down",
	"turn",
	"menu"
);

left.addKey(	0, ord("A")		);
right.addKey(	0, ord("D")		);
up.addKey(		0, ord("W")		);
down.addKey(	0, ord("S")		);
turn.addKey(	0, vk_space		);
turn.addKey(	1, mb_left		);
turn.addKey(	2, gp_face1, 0	);
menu.addKey(	0, vk_escape	);
menu.addKey(	1, mb_right		);