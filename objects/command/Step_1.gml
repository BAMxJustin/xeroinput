for( var i = 0; i < ds_list_size( list); ++i){
	var input = list[| i];
	
	input.pressed = false;
	input.released = false;
	
	for( var j = 0; j < ds_list_size( input.check); ++j){
		var c = input.check[| j];
		if( c.pressed()){
			input.pressed = true;
			input.down = true;
		}
		if( c.released()){
			input.released = true;
			input.down = false;
		}
	}
}