/// @description Created by @Xerokul / u/amnesia_sc

function Input() constructor {
	static __Key = function( _device, _keycode, _gamepadId) constructor {
		gamepadId = _gamepadId;
		keycode = _keycode;
			
		pressed = function(){
			return pressedF( keycode);
		}
		released = function(){
			return releasedF( keycode);
		}
			
		// "private"
		pressedF = undefined;
		releasedF = undefined;
			
		switch( _device){
			case 0:
				pressedF = keyboard_check_pressed;
				releasedF = keyboard_check_released;
				break;
			case 1:
				pressedF = mouse_check_button_pressed;
				releasedF = mouse_check_button_released;
				break;
			case 2:
				pressedF = function( buttonId){
					return gamepad_button_check_pressed( gamepadId, buttonId);
				};
				releasedF = function( buttonId){
					return gamepad_button_check_released( gamepadId, buttonId);
				};
			default:
				break;
		}
	}
	
	down		= false;
	pressed		= false;
	released	= false;
	
	check = ds_list_create();
	
	/// @function addKey
	/// @param {real} device		0-Keyboard, 1-Mouse, 2-Gamepad
	/// @param {real} keycode		Key or button constant
	/// @param {real} gamepadId*	Gamepad slot
	/// @description Adds a hotkey for this game action
	static addKey = function( _device, _keycode, gamepadId){
		ds_list_add( check, new __Key( _device, _keycode, gamepadId));
	}
	
	/// @function rebindKey
	/// @param {real}	slot
	/// @param {real}	device		0-Keyboard, 1-Mouse, 2-Gamepad
	/// @param {real}	keycode
	/// @param {real}	gamepadId
	/// @description Rebinds slot to new keybind
	static rebindKey = function( slot, _device, _keycode, gamepadId){
		ds_list_replace( check, slot, new __Key( _device, _keycode, gamepadId));
	}
	
	/// @function removeKey
	/// @param {real} slot	Slot
	/// @description Remove given slot
	static removeKey = function( slot){
		ds_list_delete( check, slot);
	}
}

/// @function add
/// @param {string} name	Name of command
/// @description Adds a command for given action
function add( name){
	for( var i = 0; i < argument_count; ++i){
		var input = new Input();
		variable_instance_set( self, argument[i], input);
		ds_list_add( list, input);
	}
}

list = ds_list_create();